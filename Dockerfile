FROM openjdk:8-jdk-alpine

VOLUME /tmp

ADD build/libs/cinematographer-web-1.0-SNAPSHOT.jar cinematographer-web.jar

ARG DECRYPTION_KEY

ENV DECRYPTION_KEY=$DECRYPTION_KEY

ENV PORT 8080

EXPOSE $PORT

ENTRYPOINT [ "java", "-jar", "-Dserver.port=${PORT}", "-Djasypt.encryptor.password=${DECRYPTION_KEY}", "/cinematographer-web.jar" ]
