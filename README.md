# cinematographer-web



**Idea**

**Objectives**

**Technologies**

**Requirements**

**Demo**



## Idea



A cinematographer needs a website to start showing its portfolio so everyone can see it and can also contact him.



The website will be structured in video categories, that are the following:

- Personal videos
- Music videos
- Corporate videos
- All projects



Every category will contain the videos + social media links, having 2x2 rows. The only exception will be the all projects category which will group the videos by categories.

The videos will be extracted from Youtube by scripting them right behind thumbnails, having every thumbnail a hover effect with the title of the video and allowing the user to click on it to start playing the Youtube's video.

The home page will show a compilation video from all of the cinematographer projects, an about section, and links to social networks.

A contact form page will also be created to send an email to the cinematographer with job offers or contact information using an special email created for the web page.



## Objectives



The main objective for this project is to show all the cinematographer portfolio so he everyone can check it, but also, to decrease the web cost using open source frameworks, free technologies and finding the best hosting.

This website also helps the developer in gaining experience in front development and increase its portfolio.

## Technologies



- Bootstrap: For creating the HTML views and to style them.
- Gradle: To manage the Java project and its dependencies.
- Spring: As it is the common web framework in Java to create MVCs and provides an excellent DI container.
- Thymeleaf: For managing the views and creating the contact form to receive the user information.
- Javascript: For scripting the videos opening part and to remove the sticky effect in mobile phones.
- CSS: Just to style the website.
- GreenMail: To test the Email Service.
- Docker: For providing a generic way of starting the web page.



**Requirements**

- Minimalistic website with some effects

- Categories for each video

- Removing the default Youtube's iframe

- Contact form with email sending


## Demo

Here you can find a working demo hosted with the free tier from Heroku.

http://cinematographer-website.herokuapp.com/
