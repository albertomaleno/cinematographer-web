// Removes sticky effect on smaller screens
$(function () {
    var windowWidth = window.screen.width < window.outerWidth ?
        window.screen.width : window.outerWidth;
    var mobile = windowWidth < 800;

    if (mobile) {
        var element = document.getElementById("title");
        element.classList.remove("sticky-top");
    }

});