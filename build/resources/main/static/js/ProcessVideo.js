if (!Element.prototype.requestFullscreen) {
    Element.prototype.requestFullscreen = Element.prototype.mozRequestFullscreen || Element.prototype.webkitRequestFullscreen || Element.prototype.msRequestFullscreen;
}

// listen for clicks
document.addEventListener('click', function (event) {

    // check if clicked element is a video thumbnail
    var videoId = event.target.getAttribute('data-video');
    const YOUTUBE_LINK = "https://www.youtube.com/embed/#VIDEO?wmode=transparent&autoplay=1&rel=0&controls=1&enablejsapi=1&html5=1";
    const YOUTUBE_VIDEO_ID = "mPyLF7NxZyY";

    if (!videoId) return;

    // create iframe
    var iframe = document.createElement('div');
    iframe.innerHTML = '<p>x</p><iframe id="yt-video" width="500px" height="350px" src="https://www.youtube.com/embed/' +
        videoId + '?rel=0&autoplay=1" frameborder="0" allow="accelerometer; allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    var video = iframe.childNodes[1];

    // replace the image with the video
    event.target.parentNode.replaceChild(video, event.target);

    // remove elements
    var detailsElement = document.getElementById("details");
    detailsElement.remove();
    var tiles = document.getElementById("tiles");
    document.getElementById("tiles").classList.remove('tiles');

}, false);