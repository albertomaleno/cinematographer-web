context('Actions', () => {
    const endpoint = Cypress.env('endpoint')
    beforeEach(() => {
        cy.visit(endpoint+'/all-projects-category')
    })

    it('All projects category should be titled "All projects"', () => {
        cy
            .title()
            .should('eq', 'All projects')
    })


    it('every image should be clickable and should open youtube video', () => {

        for (let i = 1; i < 13; i++) {

            cy.get('div[name="video-container"][id="0' + i + '"]')
                .find('img')
                .as('videoContainer')

            cy.get('@videoContainer')
                .invoke('attr', 'data-video')
                .then($videoId => {
                    const videoId = $videoId

                    cy.get('@videoContainer')
                        .click('center')

                    cy.get('iframe[id="yt-video"][src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1"]')
                })
        }
    })

    it('a video should have a similar height to its container image', () => {

            cy.get('div[name="video-container"][id="01"]')
                .find('img')
                .as('videoContainer')

            cy.get('@videoContainer')
                .invoke('height')
                .then(containerHeight =>{

                    cy.get('@videoContainer')
                        .click('center')
                        .invoke('attr', 'data-video').then($videoId => {
                        const videoId = $videoId


                        cy
                            .get('iframe[id="yt-video"][src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1"]')
                            .invoke('height')
                            .should('lte', containerHeight)
                            .should('gte', containerHeight * 0.90)

                    })

                })

    })


    it('media button links should be clickable', () => {
        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'href').and('include', 'mailto:jordi.heppell@gmail.com')

        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'href').and('include', 'https://www.linkedin.com/in/jordi-torrens-heppell-01b06477?originalSubdomain=uk')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'href').and('include', 'https://www.youtube.com/channel/UCSWMX29VhlmnBBHahQSdE4g/feed')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'href').and('include', 'https://vimeo.com/user41069630')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'target').and('include', '_blank')
    })


})