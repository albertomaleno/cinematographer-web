context('Actions', () => {
    const endpoint = Cypress.env('endpoint')
    beforeEach(() => {
        cy.visit(endpoint)
    })

    it('home should be titled "Home"', () => {
        cy
            .title()
            .should('eq', 'Home')
    })

    it('home button should go home', () => {
        cy.get('a.btn-circle')
            .contains('HOME')
            .click()
    })

    it('header should have a static position', () => {
        cy.get('div[class*="position-static"]')
    })


    it('title click should go to all projects', () => {
        cy.get('div')
            .contains('Jordi Torrens Heppell')
            .click()

        cy.url()
            .should('include', '/all-projects-category')
    })


    it('contact button should go to contact', () => {
        cy.get('a.btn-circle')
            .contains('CONTACT')
            .click()

        cy.url()
            .should('include', '/contact')
    })


    it('corporate button should go to corporate category', () => {
        cy.get('a.btn-circle')
            .contains('PORTFOLIO')
            .invoke('show')
            .get('a.mt-2')
            .contains('corporate videos')
            .click({
                force: true
            })

        cy.url()
            .should('include', '/corporate-category')
    })

    it('music button should go to music category', () => {
        cy.get('a.btn-circle')
            .contains('PORTFOLIO')
            .invoke('show')
            .get('a.mt-2')
            .contains('music videos')
            .click({
                force: true
            })

        cy.url()
            .should('include', '/music-category')
    })

    it('personal button should go to personal projects', () => {
        cy.get('a.btn-circle')
            .contains('PORTFOLIO')
            .invoke('show')
            .get('a.mt-2')
            .contains('personal videos')
            .click({
                force: true
            })

        cy.url()
            .should('include', '/personal-category')
    })

    it('all projects button should go to all projects category', () => {
        cy.get('a.btn-circle')
            .contains('PORTFOLIO')
            .invoke('show')
            .get('a.mt-2')
            .contains('all projects')
            .click({
                force: true
            })
    })


    it('media button links should be clickable', () => {
        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'href').and('include', 'mailto:jordi.heppell@gmail.com')

        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'href').and('include', 'https://www.linkedin.com/in/jordi-torrens-heppell-01b06477?originalSubdomain=uk')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'href').and('include', 'https://www.youtube.com/channel/UCSWMX29VhlmnBBHahQSdE4g/feed')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'href').and('include', 'https://vimeo.com/user41069630')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'target').and('include', '_blank')
    })

    it('chat link should go to contact form', () => {
        cy.get('[id=about]')
            .find('a')
            .should('have.attr', 'href').and('include', '/contact')

        cy.get('[id=about]')
            .find('a')
            .click()

        cy.url()
            .should('include', '/contact')
    })

    it('all projects link from about should go to all projects', () => {
        cy.get('div[class^="col-md-8"]')
            .find('a')
            .as('viewAll')

        cy.get('@viewAll')
            .should('have.attr', 'href').and('include', '/all-projects-category')

        cy.get('@viewAll')
            .click()

        cy.url()
            .should('include', '/all-projects-category')
    })

    it('showreel image should be clickable and should open youtube video', () => {
        cy.get('div[name="video-container"][id="01"]')
            .find('img')
            .as('videoContainer')

        cy.get('@videoContainer')
            .invoke('attr', 'data-video')
            .then($videoId => {
                const videoId = $videoId

                cy.get('@videoContainer')
                    .click()

                cy.get('iframe[id="yt-video"][src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1"]')
            })
    })


    it('showreel video should be opened in mobile phones with appropiate height', () => {
        cy.viewport(768, 500);

        cy.get('div[name="video-container"][id="01"]')
            .find('img')
            .as('videoContainer')


        cy.get('@videoContainer')
            .invoke('height')
            .then(($height) => {

                cy.get('@videoContainer')
                    .invoke('attr', 'data-video')
                    .then($videoId => {
                        const videoId = $videoId

                        cy.get('@videoContainer')
                            .click()

                        cy.get('iframe[id="yt-video"][src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1"]')
                            .invoke('height')
                            .should('lte', $height).and('gte', $height / 2)
                    })

            })

    })


})