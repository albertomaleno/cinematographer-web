context('Actions', () => {
    const endpoint = Cypress.env('endpoint')
    beforeEach(() => {
        cy.visit(endpoint+'/corporate-category')
    })

    it('Corporate category should be titled "Corporate projects"', () => {
        cy
            .title()
            .should('eq', 'Corporate projects')
    })

    it('first image should be clickable and should open youtube video', () => {

        for (let i = 1; i < 6; i++) {

            cy.get('div[name="video-container"][id="0' + i + '"]')
                .find('img')
                .as('videoContainer')

            cy.get('@videoContainer')
                .invoke('attr', 'data-video')
                .then($videoId => {
                    const videoId = $videoId

                    cy.get('@videoContainer')
                        .click('center')

                    cy.get('iframe[id="yt-video"][src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1"]')
                })
        }
    })


    it('media button links should be clickable', () => {
        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'href').and('include', 'mailto:jordi.heppell@gmail.com')

        cy.get('footer')
            .find('a:eq(0)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'href').and('include', 'https://www.linkedin.com/in/jordi-torrens-heppell-01b06477?originalSubdomain=uk')

        cy.get('footer')
            .find('a:eq(1)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'href').and('include', 'https://www.youtube.com/channel/UCSWMX29VhlmnBBHahQSdE4g/feed')

        cy.get('footer')
            .find('a:eq(2)')
            .should('have.attr', 'target').and('include', '_blank')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'href').and('include', 'https://vimeo.com/user41069630')

        cy.get('footer')
            .find('a:eq(3)')
            .should('have.attr', 'target').and('include', '_blank')
    })


})