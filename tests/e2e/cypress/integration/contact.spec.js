context('Actions', () => {
    const endpoint = Cypress.env('endpoint')
    beforeEach(() => {
        cy.visit(endpoint+'contact')
    })

    it('Contact category should be titled "Contact"', () => {
        cy
            .title()
            .should('eq', 'Contact')
    })

    it('header should contain Get in touch', () => {
        cy.get('div[class="page-header text-center"]')
            .find('h1')
            .contains('Get in touch')
    })

    it('form should have valid labels', () => {
        cy.get('div[class="form-group text-center"]')
            .should(($div) => {
                expect($div[0]).to.contain('Email address')
                expect($div[1]).to.contain('Subject')
                expect($div[2]).to.contain('Message')
            })
    })

    it('form should have valid labels', () => {
        cy.get('div[class="form-group text-center"]')
            .should(($div) => {
                expect($div[0]).to.contain('Email address')
                expect($div[1]).to.contain('Subject')
                expect($div[2]).to.contain('Message')
            })
    })

    it('submit button should contain Contact text', () => {
        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .contains('Contact')
    })

    it('email address form should not return error if valid email is given', () => {
        cy.get('div[class="form-group text-center"]')
            .find('input[class="form-control"]:eq(0)')
            .type('blablabla@hotmail.com')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click({
                force: true
            })

        cy.get('ul')
            .children()
            .should(($li) => {
                const notExpectedValue = 'A valid email is required'

                for (let i = 0; i < 2; i++) {
                    if ($li[i].textContent == notExpectedValue) {
                        throw new Error('Validation error found when it should not return it');
                    }
                }
            })

    })

    it('email address form should return error if no valid email is given', () => {
        cy.get('div[class="form-group text-center"]')
            .find('input[class="form-control"]:eq(0)')
            .type('blablabla')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('ul')
            .children()
            .should(($li) => {
                const expectedValue = 'A valid email is required'
                let found = false;

                for (let i = 0; i < 3; i++) {

                    if ($li[i].textContent == expectedValue) {
                        found = true;
                        expect($li[i]).to.contain(expectedValue);
                        break;
                    }
                }

                if (!found) {
                    throw new Error('Validation error not found')
                }
            })

    })


    it('subject form should not return error if valid subject is given', () => {
        cy.get('div[class="form-group text-center"]:eq(1)')
            .find('textarea[class="form-control"]')
            .type('blablabla')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('ul')
            .children()
            .should(($li) => {
                const notExpectedValue = 'Subject is required'

                for (let i = 0; i < 2; i++) {
                    if ($li[i].textContent == notExpectedValue) {
                        throw new Error('Validation error found when it should not return it');
                    }
                }
            })

    })


    it('message form should not return error if message is given', () => {
        cy.get('div[class="form-group text-center"]:eq(2)')
            .find('textarea[class="form-control"]')
            .type('hello, my name is guzman')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('ul')
            .children()
            .should(($li) => {
                const notExpectedValue = 'Message is required'

                for (let i = 0; i < 2; i++) {
                    if ($li[i].textContent == notExpectedValue) {
                        throw new Error('Validation error found when it should not return it');
                    }
                }
            })

    })


    it('message form should add a contact with the email if email address is given', () => {
        cy.get('div[class="form-group text-center"]:eq(0)')
            .find('input[class="form-control"]')
            .type('manel@gmail.com')

        cy.get('div[class="form-group text-center"]:eq(2)')
            .find('textarea[class="form-control"]')
            .type('hello, my name is guzman')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('div[class="form-group text-center"]:eq(2)')
            .find('textarea[class="form-control"]')
            .contains('hello, my name is guzman')
            .contains('contact: manel@gmail.com')

    })


    it('error messages are thrown if form is not filled', () => {
        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('ul')
            .children()
            .should(($li) => {
                const errorMessages = ['The email is blank', 'Message is required', 'Subject is required'];

                let found = false;
                for (let i = 0; i < 3; i++) {

                    for (let j = 0; j < errorMessages.length; j++) {
                        if ($li[i].textContent == errorMessages[j]) {
                            found = true;
                            expect($li[i]).to.contain(errorMessages[j]);
                            break;
                        }
                    }

                }

                if (!found) {
                    throw new Error('Validation errors not found')
                }
            })

    })

    it('after filling the form it should display a thank you page', () => {
        cy.get('div[class="form-group text-center"]:eq(0)')
            .find('input[class="form-control"]')
            .type('test@gmail.com')

        cy.get('div[class="form-group text-center"]:eq(1)')
            .find('textarea[class="form-control"]')
            .type('test subject')

        cy.get('div[class="form-group text-center"]:eq(2)')
            .find('textarea[class="form-control"]')
            .type('hello, my name is guzman')

        cy.get('div[class="input text-center"]')
            .find('input[type="submit"]')
            .click()

        cy.get('p')
            .contains('Thank you! You will receive a response soon')

        // should go to home after 5 seconds
        cy
            .title()
            .should('eq', 'Thank you')

        cy.wait(5000);

        cy.url()
            .should('not.include', '/contact')
    })

})