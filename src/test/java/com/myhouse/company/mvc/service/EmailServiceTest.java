package com.myhouse.company.mvc.service;

import org.junit.*;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import static org.mockito.Mockito.*;


public class EmailServiceTest {

    private MockedStatic<Transport> mockedTransport;
    private EmailService emailService;
    private Message mockedMessage;
    private Logger mockedLogger;



    @Before
    public void setUp() {
        mockedTransport = mockStatic(Transport.class);
        mockedLogger = mock(Logger.class);
        emailService = new EmailService(mockedLogger);
        mockedMessage = mock(Message.class);
        reset(mockedLogger);
    }


    @Test
    public void can_send_email(){
        mockedTransport.when(() -> {
            Transport.send(mockedMessage);
        }).then(invocation -> {
            Object arg = invocation.getArgument(0);
            Assert.assertEquals(mockedMessage, arg);
            return null;
        });

        emailService.sendMailMessage(mockedMessage);

        verify(mockedLogger, times(1))
                .debug("Sending message with the following data {}", mockedMessage.toString());
    }

    @Test
    public void error_thrown_if_message_is_invalid(){
        final MessagingException messagingException = new MessagingException();
        mockedTransport.when(() -> {
            Transport.send(any());
        }).thenThrow(messagingException);

        emailService.sendMailMessage(mockedMessage);

        verify(mockedLogger, times(1))
                .error("Error sending contact mail", messagingException);
    }

    @After
    public void tearDown(){
        mockedTransport.close();
    }

}
