package com.myhouse.company.mvc.service;

import com.icegreen.greenmail.junit4.GreenMailRule;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.myhouse.company.mvc.configuration.EmailProperties;
import com.myhouse.company.mvc.model.ContactMessage;
import com.myhouse.company.mvc.model.EmailContactMessage;
import com.myhouse.company.mvc.model.GmailContactMessage;
import com.sun.mail.imap.IMAPStore;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import java.io.IOException;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class ITEmailServiceTest {

    @Rule
    public final GreenMailRule greenMail = new GreenMailRule(ServerSetupTest.SMTP_IMAP);
    private static final String EMAIL_FROM = "test@localhost.com";
    private static final String EMAIL_FROM_USERNAME = "test";
    private static final String EMAIL_FROM_PASSWORD = "test";
    private static final String EMAIL_TO = "test2@localhost.com";
    private static final String EMAIL_CONTACT = "contact@contact.com";
    private static final String EMAIL_CONTACT_SUBJECT = "BLABLA";
    private static final String EMAIL_CONTACT_MESSAGE = "CONTACT";
    private EmailService emailService;
    private EmailContactMessage emailContactMessage;
    private ContactMessage contactMessage;
    @Autowired private EmailProperties emailProperties;


    @BeforeClass
    public static void setUpClass(){
        System.setProperty("JASYPT_ENCRYPTOR_PASSWORD", "1234");
    }

    @Before
    public void setUp() {
        greenMail.setUser(EMAIL_FROM, EMAIL_FROM_USERNAME, EMAIL_FROM_PASSWORD);
        emailProperties.setEmailFrom(EMAIL_FROM);
        emailProperties.setEmailFromPassword(EMAIL_FROM_PASSWORD);
        emailProperties.setEmailTo(EMAIL_TO);
        contactMessage = new ContactMessage();
        contactMessage.setContactEmail(EMAIL_CONTACT);
        contactMessage.setContactMessage(EMAIL_CONTACT_MESSAGE);
        contactMessage.setContactSubject(EMAIL_CONTACT_SUBJECT);
        Session smtpSession = greenMail.getSmtp().createSession();
        emailContactMessage = new EmailContactMessage(emailProperties, contactMessage, smtpSession);
        emailService = new EmailService(LoggerFactory.getLogger(EmailService.class));
    }


    @Test
    public void email_sent_has_email_subject() throws MessagingException {
        String expectedSubject = emailContactMessage.getContactMessage().getContactSubject();

        emailService.sendMailMessage(emailContactMessage);
        Message message = greenMail.getReceivedMessages()[0];

        assertThat(message.getSubject(), equalTo(expectedSubject));
    }



    @Test
    public void email_sent_has_email_from() throws MessagingException {
        String expectedEmailFrom = emailContactMessage.getEmailProperties().getEmailFrom();

        emailService.sendMailMessage(emailContactMessage);
        Message message = greenMail.getReceivedMessages()[0];

        assertThat(message.getFrom()[0].toString(), equalTo(expectedEmailFrom));
    }

    @Test
    public void email_sent_has_email_to() throws MessagingException {
        String expectedEmailTo = emailContactMessage.getEmailProperties().getEmailTo();

        emailService.sendMailMessage(emailContactMessage);
        Message message = greenMail.getReceivedMessages()[0];

        assertThat(message.getAllRecipients()[0].toString(), equalTo(expectedEmailTo));
    }

    @Test
    public void email_sent_has_email_message() throws MessagingException, IOException {
        String expectedEmailBody = emailContactMessage.getContactMessage().getContactMessage();
        String expectedEmailContact = emailContactMessage.getContactMessage().getContactEmail();

        emailService.sendMailMessage(emailContactMessage);
        Message message = greenMail.getReceivedMessages()[0];

        assertThat(message.getContent().toString(), containsString(expectedEmailContact));
        assertThat(expectedEmailBody, equalToCompressingWhiteSpace(message.getContent().toString()));
    }

    @Test
    public void gmail_sent_has_same_gmail_data() throws MessagingException, IOException {
        GmailContactMessage gmailContactMessage = new GmailContactMessage(emailProperties, contactMessage);
        gmailContactMessage.setSession(greenMail.getSmtp().createSession());
        emailContactMessage.setSession(gmailContactMessage.getSession());

        emailService.sendMailMessage(emailContactMessage);
        Message message = greenMail.getReceivedMessages()[0];

        assertThat(gmailContactMessage.getContactMessage().getContactSubject(), equalTo(message.getSubject()));
        assertThat(message.getContent().toString(), equalToCompressingWhiteSpace(gmailContactMessage.getContactMessage().getContactMessage()));
        assertThat(gmailContactMessage.getEmailProperties().getEmailTo(), equalTo(message.getAllRecipients()[0].toString()));
        assertThat(gmailContactMessage.getEmailProperties().getEmailFrom(), equalTo(message.getFrom()[0].toString()));
    }


    @After
    public void tearDown(){
        greenMail.stop();
    }



}
