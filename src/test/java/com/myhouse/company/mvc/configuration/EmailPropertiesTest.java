package com.myhouse.company.mvc.configuration;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmailPropertiesTest {


    private EmailProperties emailProperties;

    @Before
    public void setUp() {
        emailProperties = new EmailProperties();
    }

    @Test
    public void can_load_password() {
        Assert.assertNotEquals("null", emailProperties.getEmailFromPassword());
    }

    @Test
    public void can_load_email_from() {
        Assert.assertNotEquals("null", emailProperties.getEmailFrom());
    }

    @Test
    public void can_load_email_to() {
        Assert.assertNotEquals("null", emailProperties.getEmailTo());
    }
}
