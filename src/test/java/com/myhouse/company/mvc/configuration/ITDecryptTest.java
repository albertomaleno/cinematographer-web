package com.myhouse.company.mvc.configuration;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class ITDecryptTest {


    @Value("${encrypted.test}")
    private String testPassword;

    static {
        System.setProperty("JASYPT_ENCRYPTOR_PASSWORD", "1234");
    }

    @Test
    public void can_decrypt() {
        Assert.assertEquals("testPass", testPassword);
    }
}