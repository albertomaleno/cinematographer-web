package com.myhouse.company.mvc.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

import com.myhouse.company.mvc.configuration.EmailProperties;
import com.myhouse.company.mvc.service.EmailService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class WebControllerTest {

    private WebController webController;
    private EmailService mockedEmailService;
    private EmailProperties mockedEmailProperties;
    @Autowired private MockMvc mockMvc;

    @Before
    public void setUp(){
        mockedEmailService = mock(EmailService.class);
        doNothing().when(mockedEmailService).sendMailMessage(any());
        mockedEmailProperties = mock(EmailProperties.class);
        when(mockedEmailProperties.getEmailFrom()).thenReturn("email@hotmail.com");
        when(mockedEmailProperties.getEmailTo()).thenReturn("email2@hotmail.com");
        when(mockedEmailProperties.getEmailFromPassword()).thenReturn("password");
        webController = new WebController(mockedEmailProperties, mockedEmailService);
        mockMvc = MockMvcBuilders.standaloneSetup(webController).build();
    }


    @Test
    public void can_get_home() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    @Test
    public void can_get_all_projects_category() throws Exception {
        mockMvc.perform(get("/all-projects-category/"))
                .andExpect(status().isOk())
                .andExpect(view().name("all-projects-category"));
    }

    @Test
    public void can_get_music_category() throws Exception {
        mockMvc.perform(get("/music-category/"))
                .andExpect(status().isOk())
                .andExpect(view().name("music-category"));
    }

    @Test
    public void can_get_personal_category() throws Exception {
        mockMvc.perform(get("/personal-category/"))
                .andExpect(status().isOk())
                .andExpect(view().name("personal-category"));
    }

    @Test
    public void can_get_corporate_category() throws Exception {
        mockMvc.perform(get("/corporate-category/"))
                .andExpect(status().isOk())
                .andExpect(view().name("corporate-category"));
    }

    @Test
    public void can_get_contact() throws Exception {
        mockMvc.perform(get("/contact/"))
                .andExpect(status().isOk())
                .andExpect(view().name("contact"));
    }

    @Test
    public void can_post_contact() throws Exception {
        mockMvc.perform(post("/contact/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content("contactEmail=blabla@gmail.com&contactSubject=contactSubject&contactMessage=blablabala"))
        .andExpect(forwardedUrl("thank-you"));
    }

    @Test
    public void if_email_is_sent_from_localhost_email_to_property_is_changed() throws Exception {
        doNothing().when(mockedEmailProperties).setEmailTo(anyString());

        mockMvc.perform(post("/contact/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("contactEmail=blabla@gmail.com&contactSubject=contactSubject&contactMessage=blablabala"))
                .andExpect(forwardedUrl("thank-you"));

        verify(mockedEmailProperties, times(1)).setEmailTo(anyString());
    }

    @Test
    public void empty_error_posting_contact_if_no_contact_mail() throws Exception {
        mockMvc.perform(post("/contact/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("contactEmail=&contactSubject=contactSubject&contactMessage=blablabala"))
                .andExpect(model()
                        .attributeHasFieldErrorCode("contactMessage",
                                "contactEmail", "NotEmpty"));
    }

    @Test
    public void validation_error_posting_contact_if_contact_mail_is_invalid() throws Exception {
        mockMvc.perform(post("/contact/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("contactEmail=bbbb&contactSubject=contactSubject&contactMessage=blablabala"))
                .andExpect(model()
                        .attributeHasFieldErrorCode("contactMessage",
                                "contactEmail", "Email"));
    }

    @Test
    public void empty_error_posting_contact_if_no_contact_subject() throws Exception {
        mockMvc.perform(post("/contact/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("contactEmail=blabla@gmail.com&contactSubject=&contactMessage=blablabala"))
                .andExpect(model()
                        .attributeHasFieldErrorCode("contactMessage",
                                "contactSubject", "NotEmpty"));
    }

    @Test
    public void empty_error_posting_contact_if_no_message() throws Exception {
        mockMvc.perform(post("/contact/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("contactEmail=blabla@gmail.com&contactSubject=aaaaa&contactMessage="))
                .andExpect(model()
                        .attributeHasFieldErrorCode("contactMessage",
                                "contactMessage", "NotEmpty"));
    }
}

