document.addEventListener('click', function (event) {

    // Listens for clicks, if the click is done by a touch screen, then it removes the href from portfolio
    if ((('ontouchstart' in window) ||
        (navigator.maxTouchPoints > 0) ||
        (navigator.msMaxTouchPoints > 0)))
    {
        var portfolioButton = document.getElementById('portfolio-button');
        portfolioButton.removeAttribute("href");
    }

}, false);
