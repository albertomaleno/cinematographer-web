const videoAttribute = 'data-video';
const homeVideo = 'FjBrVAEAie8';
const mobileWidth = 768;
const videoWidth = "100%";

document.addEventListener('click', function (event) {
    const videoId = event.target.getAttribute(videoAttribute);
    if(!isVideoThumbnail(videoId)) return;

    let videoHeight = event.target.clientHeight * 0.999;

    const iframe = createVideoFrame(videoWidth, videoHeight, videoId);
    const video = iframe.childNodes[1];
    event.target.parentNode.replaceChild(video, event.target);
}, false);


function isVideoThumbnail(videoId){
    return !!videoId;
}


function createVideoFrame(videoWidth, videoHeight, videoId){
    const iframe = document.createElement('div');
    iframe.innerHTML = '<p>x</p><iframe id="yt-video" width=' + videoWidth + ' height=' + videoHeight + ' src="https://www.youtube.com/embed/' +
        videoId + '?rel=0&autoplay=1" frameborder="0" allow="accelerometer; allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    return iframe;
}