function removeOverlay(videoContainerId, videoOverlayId) {
    document.getElementById(videoContainerId).classList.remove("hover-effect");
    document.getElementById(videoOverlayId).remove();
}