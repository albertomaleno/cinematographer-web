package com.myhouse.company.mvc.model;


import com.myhouse.company.mvc.configuration.EmailProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;


public class GmailContactMessage extends EmailContactMessage {

    private static final Properties PROPERTIES;

    static {
        PROPERTIES = new Properties();
        PROPERTIES.put("mail.smtp.auth", "true");
        PROPERTIES.put("mail.smtp.starttls.enable", "true");
        PROPERTIES.put("mail.smtp.host", "smtp.gmail.com");
        PROPERTIES.put("mail.smtp.port", "587");
    }

    public GmailContactMessage(@Autowired EmailProperties emailProperties, ContactMessage contactMessage) {
        super(emailProperties, contactMessage, Session.getInstance(PROPERTIES, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailProperties.getEmailFrom(), emailProperties.getEmailFromPassword());
            }
        }));

    }
}
