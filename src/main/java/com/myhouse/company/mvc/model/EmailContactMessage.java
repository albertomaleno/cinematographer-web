package com.myhouse.company.mvc.model;

import com.myhouse.company.mvc.configuration.EmailProperties;
import org.springframework.beans.factory.annotation.Autowired;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class EmailContactMessage extends MimeMessage {


    private EmailProperties emailProperties;
    private ContactMessage contactMessage;
    private Session session;


    public EmailContactMessage(@Autowired EmailProperties emailProperties, ContactMessage contactMessage, Session session) {
        super(session);
        this.emailProperties = emailProperties;
        this.contactMessage = contactMessage;
        this.session = session;
        content = contactMessage.getContactMessage().getBytes();
        headers.setHeader("From", emailProperties.getEmailFrom());
        headers.setHeader("To", emailProperties.getEmailTo());
        headers.setHeader("Subject", contactMessage.getContactSubject());
    }

    public EmailProperties getEmailProperties() {
        return emailProperties;
    }

    public void setEmailProperties(EmailProperties emailProperties) {
        this.emailProperties = emailProperties;
    }

    public ContactMessage getContactMessage() {
        return contactMessage;
    }

    public void setContactMessage(ContactMessage contactMessage) {
        this.contactMessage = contactMessage;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public String toString() {
        return "EmailContactMessage{" +
                "contactMessage=" + contactMessage +
                '}';
    }
}
