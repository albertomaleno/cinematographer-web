package com.myhouse.company.mvc.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class ContactMessage {

    @NotEmpty(message = "The email is blank")
    @Email(message = "A valid email is required")
    private String contactEmail;
    @NotEmpty(message = "Subject is required")
    private String contactSubject;
    @NotEmpty(message = "Message is required")
    private String contactMessage;

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactSubject() {
        return contactSubject;
    }

    public void setContactSubject(String contactSubject) {
        this.contactSubject = contactSubject;
    }

    public String getContactMessage() {
        return contactMessage != null ? (contactMessage + System.lineSeparator()
                        + "contact: " + contactEmail) : null;
    }

    public void setContactMessage(String contactMessage) {
        this.contactMessage = contactMessage;
    }

    @Override
    public String toString() {
        return "ContactMessage{" +
                "contactEmail='" + contactEmail + '\'' +
                ", contactSubject='" + contactSubject + '\'' +
                ", contactMessage='" + contactMessage + '\'' +
                '}';
    }
}
