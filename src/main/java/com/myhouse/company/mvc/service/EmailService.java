package com.myhouse.company.mvc.service;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

@Service
public class EmailService {

    private Logger logger;

    public EmailService(Logger logger){
        this.logger = logger;
    }

    public void sendMailMessage(Message message){
        logger.debug("Sending message with the following data {}", message.toString());
        try{
            Transport.send(message);
        }catch (MessagingException e){
            logger.error("Error sending contact mail", e);
        }
    }
}
