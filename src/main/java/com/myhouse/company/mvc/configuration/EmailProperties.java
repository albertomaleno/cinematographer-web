package com.myhouse.company.mvc.configuration;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@EnableEncryptableProperties
public class EmailProperties {

    @Value("${email.password}")
    private String password;
    @Value("${email.from}")
    private String emailFrom;
    @Value("${email.to}")
    private String emailTo;

    public String getEmailFromPassword() {
        return password;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailFromPassword(String password) {
        this.password = password;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }
}
