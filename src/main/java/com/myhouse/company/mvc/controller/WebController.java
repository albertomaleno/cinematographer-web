package com.myhouse.company.mvc.controller;

import com.myhouse.company.mvc.configuration.EmailProperties;
import com.myhouse.company.mvc.model.ContactMessage;
import com.myhouse.company.mvc.model.GmailContactMessage;
import com.myhouse.company.mvc.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
public class WebController {

    private final EmailProperties emailProperties;
    private final EmailService emailService;

    public WebController(@Autowired EmailProperties emailProperties, EmailService emailService){
        this.emailProperties = emailProperties;
        this.emailService = emailService;
    }

    @GetMapping("/")
    public String getHome() {
        return "home";
    }

    @GetMapping("/all-projects-category")
    public String getPortfolio() {
        return "all-projects-category";
    }

    @GetMapping("/music-category")
    public String getMusicPortfolio() {
        return "music-category";
    }

    @GetMapping("/personal-category")
    public String getPersonalPortfolio() {
        return "personal-category";
    }

    @GetMapping("/corporate-category")
    public String getCorporatePortfolio() {
        return "corporate-category";
    }

    @GetMapping("/contact")
    public String getContactForm(Model model){
        model.addAttribute("contactMessage", new ContactMessage());
        return "contact";
    }

    @PostMapping("/contact")
    public String postContact(@Valid ContactMessage contactMessage, Errors errors, HttpServletRequest request){
        if (errors.hasErrors()) return "contact";
        if (request.getRemoteAddr().equals("127.0.0.1")) emailProperties.setEmailTo("nobody@@");
        GmailContactMessage gmailContactMessage = new GmailContactMessage(emailProperties, contactMessage);
        emailService.sendMailMessage(gmailContactMessage);
        return "thank-you";
    }
}



