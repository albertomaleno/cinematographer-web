#!/bin/sh

# download image from gitlab registry
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
docker pull "$RELEASE_IMAGE_TAG"
docker logout

# push image to heroku registry
docker login -u "$HEROKU_AUTH_USERNAME" -p "$HEROKU_AUTH_TOKEN" "$HEROKU_REGISTRY"
docker tag "$RELEASE_IMAGE_TAG" "$HEROKU_TAG"
docker push "$HEROKU_TAG"

if [ $? != 0 ]; then exit 1; fi;

# trigger deploy
curl -X PATCH https://api.heroku.com/apps/cinematographer-website/formation --header "Content-Type: application/json" \
--header "Accept: application/vnd.heroku+json; version=3.docker-releases" \
--header "Authorization: Bearer ${HEROKU_AUTH_TOKEN}" \
--data '{ "updates": [ { "type": "web", "docker_image": "'$(docker inspect $RELEASE_IMAGE_TAG --format={{.Id}})'" } ] }'
